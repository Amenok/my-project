package fi.u.chiliappprojekti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import fi.u.chiliappprojekti.R;

public class MenuActivity extends AppCompatActivity { //Menu activity listing Capsicum species

    ListView myListView;
    String[] species;
    String[] descriptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Resources res = getResources();
        myListView = (ListView) findViewById(R.id.myListView);
        species = res.getStringArray(R.array.species);
        descriptions = res.getStringArray(R.array.descriptions);


        ItemAdapter itemAdapter = new ItemAdapter(this, species, descriptions);
        myListView.setAdapter(itemAdapter);
        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent showDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);
                showDetailActivity.putExtra("fi.u.ITEM_INDEX", position);
                startActivity(showDetailActivity);

            }

        });
    }
}
