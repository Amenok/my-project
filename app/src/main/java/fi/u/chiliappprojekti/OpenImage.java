package fi.u.chiliappprojekti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;

import fi.u.chiliappprojekti.R;

public class OpenImage extends AppCompatActivity {
//pretty much straight from 3rd tutorial
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_image);
        Intent in = getIntent();
        int index = in.getIntExtra("fi.u.IMAGE_INDEX", -1);

        int pic = getPic(index);
        ImageView img = findViewById(R.id.openImageView);
        scaleImg(img, pic);
    }
    //returns correct image according to index
    private int getPic(int index) {

        switch (index) {
            case 0:return R.drawable.annuum;
            case 1: return R.drawable.baccatum;
            case 2: return R.drawable.chinense;
            case 3:return R.drawable.frutescens;
            case 4: return R.drawable.pubescens;
            case 5: return R.drawable.villit;
            default: return -1;
        }
    }
    //Scales image to fit screen
    //from 3rd tutorial video
    private void scaleImg(ImageView img, int pic) {
        Display screen = getWindowManager().getDefaultDisplay();
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), pic, options);

        int imgWidth = options.outWidth;
        int screenWidth = screen.getWidth();

        if (imgWidth > screenWidth) {
            int ratio = Math.round((float) imgWidth / (float) screenWidth);
            options.inSampleSize = ratio;
        }
        options.inJustDecodeBounds = false;
        Bitmap scaledImg = BitmapFactory.decodeResource(getResources(), pic, options);
        img.setImageBitmap(scaledImg);
    }
}