package fi.u.chiliappprojekti;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import fi.u.chiliappprojekti.R;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent in = getIntent();
        int index = in.getIntExtra("fi.u.ITEM_INDEX", -1); //index from species listView
        int menu = in.getIntExtra("fi.u.MENU_INDEX", -1); //index from menu listView
        //Gets the correct information according to list item index from species menu
        if (index > -1) {
            String[] species;
            String[] info;
            TextView titleTextView;
            TextView infoTextView;
            titleTextView = (TextView) findViewById(R.id.titleTextView);
            infoTextView = (TextView) findViewById(R.id.infoTextView);
            Resources res = getResources();
            info = res.getStringArray(R.array.info);
            species = res.getStringArray(R.array.species);

            //selects correct string using index and sets it to correct textView
            String name = species[index];
            String infoString = info[index];
            titleTextView.setText(name);
            infoTextView.setText(infoString);

            int pic = getPic(index);
            ImageView img = (ImageView) findViewById(R.id.imageView);
            scaleImg(img, pic); //gets picture according to item index and scales it to fit on phone

            //calls for right wiki page, site argument changes used stringarray for links and is set as text on button
            gotoWikiLink(index, "wiki");
            openImage(index); //Calls openImage() to enable clicking picture to open in new activity


        }
        //Gets the correct information according to list item index from main menu
        //as above
        if (menu > -1) {
            String[] mainOptions;
            String[] optionInfo;

            TextView titleTextView;
            TextView infoTextView;
            titleTextView = (TextView) findViewById(R.id.titleTextView);
            infoTextView = (TextView) findViewById(R.id.infoTextView);

            Resources res = getResources();
            optionInfo = res.getStringArray(R.array.optionInfo);
            mainOptions = res.getStringArray(R.array.mainOptions);

            String name = mainOptions[menu];
            String infoString = optionInfo[menu];

            titleTextView.setText(name);
            infoTextView.setText(infoString);


            ImageView img = (ImageView) findViewById(R.id.imageView);
            scaleImg(img, -1); //set to -1 to default and hide the image placeholder

            //calls for right wiki page, site argument changes used stringarray for links and is set as text on button
            gotoWikiLink(menu, "inferno.net");



        }

    }






    //Returns correct picture according to index of list item
    private int getPic(int index) {

        switch (index) {
            case 0: return R.drawable.annuum;
            case 1: return R.drawable.baccatum;
            case 2: return R.drawable.chinense;
            case 3: return R.drawable.frutescens;
            case 4: return R.drawable.pubescens;
            case 5: return R.drawable.villit;
            default: return -1;
        }
    }
    //opens the picture in full screen in new activity when clicking
    private void openImage(int index){
        final int imageNumber = index;
        ImageView imageView;
        imageView = (ImageView)findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent showImageActivity = new Intent(getApplicationContext(), OpenImage.class);
                showImageActivity.putExtra("fi.u.IMAGE_INDEX", imageNumber);
                startActivity(showImageActivity);
            }
        });
    }
    //scales images to fit screen.
    private void scaleImg(ImageView img, int pic) {
        Display screen = getWindowManager().getDefaultDisplay();
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(getResources(), pic, options);

        int imgWidth = options.outWidth;
        int screenWidth = screen.getWidth();

        if (imgWidth > screenWidth) {
            int ratio = Math.round((float) imgWidth / (float) screenWidth);
            options.inSampleSize = ratio;
        }
        options.inJustDecodeBounds = false;
        Bitmap scaledImg = BitmapFactory.decodeResource(getResources(), pic, options);
        img.setImageBitmap(scaledImg);


    }
    //Clicking button takes you to correct wiki page for the species
    //site argument changes which stringarray to use and is set as text on the button
    private void gotoWikiLink(int index, String site){
        Button wikiLinkBtn;
        String[] infernoLinks;
        String[] wikiLinks;
        String[] links;

        wikiLinkBtn = (Button) findViewById(R.id.wikiLinkBtn);
        wikiLinks = getResources().getStringArray(R.array.wikiLinks);
        infernoLinks = getResources().getStringArray(R.array.infernoLinks);

        wikiLinkBtn.setText(site); //Sets site as text on button
        if (site =="wiki")
            links = wikiLinks;
        else
            links = infernoLinks;

        final String Link = links[index];

        wikiLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webaddress = Uri.parse(Link);
                Intent gotoWiki = new Intent(Intent.ACTION_VIEW, webaddress);
                if (gotoWiki.resolveActivity(getPackageManager()) != null) {
                    startActivity(gotoWiki);
                }
            }
        });
    }
}
