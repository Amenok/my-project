package fi.u.chiliappprojekti;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import fi.u.chiliappprojekti.R;

public class ItemAdapter extends BaseAdapter { //Adapter for items in list for species listView

    LayoutInflater mInflater;
    String[] species;
    String[] descriptions;

    public ItemAdapter(Context c, String[] s, String[] d){
        species = s;
        descriptions = d;
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    @Override
    public int getCount() {
        return species.length;
    }

    @Override
    public Object getItem(int position) {
        return species[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = mInflater.inflate(R.layout.species_listview_detail, null);
        //Finds wanted textViews
        TextView nameTextView = (TextView) v.findViewById(R.id.nameTextView);
        TextView descriptionTextView = (TextView) v.findViewById(R.id.descriptionTextView);
        //picks up string according to index
        String name = species[position];
        String desc = descriptions[position];
        //sets the string to textView
        nameTextView.setText(name);
        descriptionTextView.setText(desc);


        return v;
    }
}
