package fi.u.chiliappprojekti;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import fi.u.chiliappprojekti.R;

public class MainActivity extends AppCompatActivity {

    ListView myMainListView;
    String[] mainOptions;
    String[] optionDescriptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //gets resources
        Resources res = getResources();
        myMainListView = (ListView) findViewById(R.id.myMainListView);
        mainOptions = res.getStringArray(R.array.mainOptions);
        optionDescriptions = res.getStringArray(R.array.optionDescriptions);
        //adapts items to listView. Own adapter since the listViews are different
        OptionAdapter optionAdapter = new OptionAdapter(this, mainOptions, optionDescriptions);
        myMainListView.setAdapter(optionAdapter);
        myMainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Each list item has it's own intent and activity it will do
                    if (position == 0) {
                        Intent showMenuActivity = new Intent(getApplicationContext(), MenuActivity.class);
                        startActivity(showMenuActivity);
                    }
                    if (position == 1) {
                        Intent showDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);
                        showDetailActivity.putExtra("fi.u.MENU_INDEX", position);
                        startActivity(showDetailActivity);
                    }
                    if (position == 2) {
                        Intent showDetailActivity = new Intent(getApplicationContext(), DetailActivity.class);
                        showDetailActivity.putExtra("fi.u.MENU_INDEX", position);
                        startActivity(showDetailActivity);

                    }
                    if (position == 3) {
                        String forum = "http://www.chilifoorumi.fi/index.php";
                        Uri webaddress = Uri.parse(forum);
                        Intent gotoForum = new Intent(Intent.ACTION_VIEW, webaddress);
                        if (gotoForum.resolveActivity(getPackageManager()) != null) {
                            startActivity(gotoForum);
                        }
                    }
                    if (position == 4) {
                        String shop = "https://www.fataliiseeds.net";
                        Uri webaddress = Uri.parse(shop);
                        Intent gotoShop = new Intent(Intent.ACTION_VIEW, webaddress);
                        if (gotoShop.resolveActivity(getPackageManager()) != null) {
                            startActivity(gotoShop);
                        }
                    }

            }
            });



        }


    }

