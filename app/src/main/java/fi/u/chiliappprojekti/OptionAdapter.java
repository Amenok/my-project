package fi.u.chiliappprojekti;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import fi.u.chiliappprojekti.R;

public class OptionAdapter extends BaseAdapter {//Another adapter to adapt main menu

    LayoutInflater mInflater;
    String[] mainOptions;
    String[] optionDescriptions;

    public OptionAdapter(Context c, String[] o, String[] d){
        mainOptions = o;
        optionDescriptions = d;
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return mainOptions.length;
    }

    @Override
    public Object getItem(int position) {
        return mainOptions[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View m = mInflater.inflate(R.layout.mainmenu_listview_detail, null);
        TextView optionTextView = (TextView) m.findViewById(R.id.optionTextView);
        TextView optionDescriptionTextView = (TextView) m.findViewById(R.id.optionDescriptionTextView);

        String option = mainOptions[position];
        String optionDesc = optionDescriptions[position];

        optionTextView.setText(option);
        optionDescriptionTextView.setText(optionDesc);

        return m;
    }
}
